#!/usr/bin/make -f

PACKAGE = hypra-launchers

ALL_LINGUAS = $(shell cat po/LINGUAS)
PO_FILES = $(ALL_LINGUAS:%=po/%.po)

prefix = /usr
datadir = $(prefix)/share
localedir = $(datadir)/locale

all: Makefile po/LINGUAS $(PO_FILES)

clean:
	$(RM) $(PO_FILES:.po=.mo)
	$(RM) po/$(PACKAGE).pot

install: $(PO_FILES:.po=.mo)
	for l in $(ALL_LINGUAS); do \
		install -m 755 -d $(DESTDIR)/$(localedir)/$$l/LC_MESSAGES || exit 1; \
		install -m 644 po/$$l.mo $(DESTDIR)/$(localedir)/$$l/LC_MESSAGES/$(PACKAGE).mo || exit 1; \
	done

po/$(PACKAGE).pot: po/POTFILES $(shell sed 's/\(^\|\W\)-[^ ]*/\1/g' po/POTFILES) Makefile
	echo > $@.tmp  # make sure the file exists and is empty
	while read -r file_and_args; do \
		xgettext -o $@.tmp -j --package-name $(PACKAGE) --foreign-user $$file_and_args; \
	done <po/POTFILES && mv -f $@.tmp $@ || rm -f $@.tmp

po/%.po: po/$(PACKAGE).pot Makefile
	if test  -f $@; then \
		msgmerge -U $@ $<; \
	else \
		msginit -i $< -o $@ -l "$(shell printf '%s' "$@" | sed 's%.*/\([^.]*\).po$$%\1%')"; \
	fi

%.mo: %.po Makefile
	msgfmt --check --check-accelerators=_ -o $@ $<
